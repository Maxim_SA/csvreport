package dao

import (
	"github.com/jmoiron/sqlx"
	"database/sql"
)

type LastUsersWinDatesResult struct {
	LastWinDate sql.NullString `db:"last_win_date"`
	UserId      sql.NullInt64  `db:"user_id"`
}

type LastUsersBidDatesResult struct {
	LastBidDate sql.NullString `db:"last_bid_date"`
	UserId      sql.NullInt64  `db:"user_id"`
}

type LastUsersPaymentDatesResult struct {
	LastPaymentDate sql.NullString `db:"last_payment_date"`
	UserId      sql.NullInt64  `db:"user_id"`
}

func GetALLLastUsersBidDates(db *sqlx.DB, c chan map[int64]LastUsersBidDatesResult, userIds []int64) {

	query, args, err := sqlx.In(`SELECT
  (CONVERT_TZ(created_on, '+00:00', '-05:00')) AS last_bid_date,
  user_id
FROM bid_transaction
WHERE user_id IN (?)
      AND auction_id > 0
      AND lot_item_id > 0
      AND bid > 0
      AND (deleted = FALSE OR deleted IS NULL OR deleted = '')
ORDER BY created_on DESC`, userIds)
	if err != nil {
		panic(err.Error())
	}
	query = db.Rebind(query)
	rows, err := db.Queryx(query, args...)
	if err != nil {
		panic(err.Error())
	}

	defer rows.Close()
	result := make(map[int64]LastUsersBidDatesResult)

	for rows.Next() {
		var res LastUsersBidDatesResult

		err = rows.StructScan(&res)

		if err != nil {
			panic(err.Error())
		}

		if res.UserId.Valid {
			result[res.UserId.Int64] = res;
		}
	}

	c <- result
	close(c)
}

func GetALLLastUsersWinDates(db *sqlx.DB, c chan map[int64]LastUsersWinDatesResult, userIds []int64) {

	query, args, err := sqlx.In(`SELECT
		(CONVERT_TZ(bt.created_on, '+00:00', '-05:00')) AS last_win_date,
			user_id
		FROM bid_transaction AS bt
		LEFT JOIN lot_item AS li ON li.id = bt.lot_item_id AND li.auction_id = bt.auction_id AND bt.user_id=li.winning_bidder_id
		WHERE bt.user_id IN (?)
		AND bt.auction_id > 0
		AND bt.lot_item_id > 0
		AND bt.bid_status = 'winner'
		AND (bt.deleted = FALSE OR bt.deleted IS NULL OR bt.deleted = '')
		AND li.internet_bid > 0
		AND li.hammer_price > 0
		ORDER BY bt.created_on DESC`, userIds)
	if err != nil {
		panic(err.Error())
	}
	query = db.Rebind(query)
	rows, err := db.Queryx(query, args...)
	if err != nil {
		panic(err.Error())
	}

	defer rows.Close()
	result := make(map[int64]LastUsersWinDatesResult)

	for rows.Next() {
		var res LastUsersWinDatesResult

		err = rows.StructScan(&res)

		if err != nil {
			panic(err.Error())
		}

		if res.UserId.Valid {
			result[res.UserId.Int64] = res;
		}
	}

	c <- result
	close(c)
}

func GetALLLastUsersPaymentDates(db *sqlx.DB, c chan map[int64]LastUsersPaymentDatesResult, userIds []int64) {

	query, args, err := sqlx.In(`SELECT IF(p.paid_on != '' AND p.paid_on IS NOT NULL,
	(CONVERT_TZ(p.paid_on, '+00:00', '-05:00')),
          (CONVERT_TZ(p.created_on, '+00:00', '-05:00'))) AS last_payment_date,
  if(i.bidder_id,i.bidder_id,  s.consignor_id) as user_id
FROM payment AS p
  LEFT JOIN invoice AS i ON i.id = p.tran_id
  LEFT JOIN settlement AS s ON s.id = p.tran_id
WHERE p.amount > 0
      AND (i.bidder_id in (?) OR s.consignor_id in (?))
      AND (i.active = TRUE OR s.active = TRUE)
      AND (p.tran_id = i.id OR p.tran_id = s.id)
ORDER BY p.paid_on DESC`, userIds, userIds)
	if err != nil {
		panic(err.Error())
	}
	query = db.Rebind(query)
	rows, err := db.Queryx(query, args...)
	if err != nil {
		panic(err.Error())
	}

	defer rows.Close()
	result := make(map[int64]LastUsersPaymentDatesResult)

	for rows.Next() {
		var res LastUsersPaymentDatesResult

		err = rows.StructScan(&res)

		if err != nil {
			panic(err.Error())
		}

		if res.UserId.Valid {
			result[res.UserId.Int64] = res;
		}
	}

	c <- result
	close(c)
}
