package dao

//NO FINISHED
import (
	"github.com/jmoiron/sqlx"
	"database/sql"
)

type UserShippingResult struct {
	Id          int64          `db:"id"`
	UserId      sql.NullInt64  `db:"user_id"`
	CompanyName sql.NullString `db:"company_name"`
	FirstName   sql.NullString `db:"first_name"`
	LastName    sql.NullString `db:"last_name"`
	Phone       sql.NullString `db:"phone"`
	Fax         sql.NullString `db:"fax"`
	Country     sql.NullString `db:"country"`
	Address     sql.NullString `db:"address"`
	Address2    sql.NullString `db:"address2"`
	City        sql.NullString `db:"city"`
	State       sql.NullString `db:"state"`
	Zip         sql.NullString `db:"zip"`
	Address3    sql.NullString `db:"address3"`
	ContactType sql.NullString `db:"contact_type"`
}

func GetUserShippingByUserId(db *sqlx.DB, c chan map[int64]UserShippingResult, userIds []int64) {

	query, args, err := sqlx.In(`SELECT
		id,
		user_id,
		company_name,
		first_name,
		last_name,
		phone,
		fax,
		country,
		address,
		address2,
		city,
		state,
		zip,
		address3,
		contact_type
	FROM user_shipping WHERE user_id IN (?)`, userIds)
	if err != nil {
		panic(err.Error())
	}
	query = db.Rebind(query)
	rows, err := db.Queryx(query, args...)
	if err != nil {
		panic(err.Error())
	}

	defer rows.Close()
	result := make(map[int64]UserShippingResult)

	for rows.Next() {
		var res UserShippingResult

		err = rows.StructScan(&res)

		if err != nil {
			panic(err.Error())
		}

		if res.UserId.Valid {
			result[res.UserId.Int64] = res;
		}
	}

	c <- result
	close(c)
}
