package dao

import (
	"database/sql"
	"github.com/jmoiron/sqlx"
)

type UserCustomField struct {
	Id             int             `db:"id"`
	Name           sql.NullString  `db:"name"`
	Order          sql.NullFloat64 `db:"order"`
	CustomType     sql.NullInt64   `db:"type"`
	Panel          sql.NullString  `db:"panel"`
	Parameters     sql.NullString  `db:"parameters"`
	Active         sql.NullInt64   `db:"active"`
	CreatedOn      sql.NullString  `db:"created_on"`
	CreatedBy      sql.NullInt64   `db:"created_by"`
	ModifiedOn     sql.NullString  `db:"modified_on"`
	ModifiedBy     sql.NullInt64   `db:"modified_by"`
	Required       sql.NullInt64   `db:"required"`
	OnRegistration sql.NullInt64   `db:"on_registration"`
	OnProfile      sql.NullInt64   `db:"on_profile"`
	Encrypted      sql.NullInt64   `db:"encrypted"`
	InAdminSearch  sql.NullInt64   `db:"in_admin_search"`
	InAdminCatalog sql.NullInt64   `db:"in_admin_catalog"`
	InInvoices     sql.NullInt64   `db:"in_invoices"`
	InSettlements  sql.NullInt64   `db:"in_settlements"`
	OnAddNewBidder sql.NullInt64   `db:"on_add_new_bidder"`
}

func LoadAllEditable(db *sqlx.DB, c chan []UserCustomField) {
	rows, err := db.Queryx("SELECT * FROM user_cust_field WHERE active = 1 AND type != 9 ORDER BY 'order'")
	if err != nil {
		panic(err.Error())
	}
	var result []UserCustomField
	for rows.Next() {
		var p UserCustomField
		err = rows.StructScan(&p)
		if err != nil {
			continue
		}
		result = append(result, p)
	}
	c <- result
	close(c)
}
