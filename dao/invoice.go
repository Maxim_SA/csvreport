package dao

import (
	"github.com/jmoiron/sqlx"
	"database/sql"
)

type LastUserInvoiceDateResult struct {
	LastWinDate sql.NullString `db:"last_invoice_date"`
	UserId      sql.NullInt64  `db:"user_id"`
}

func GetLastUserInvoiceDate(db *sqlx.DB, c chan map[int64]string, userIds []int64) {

	query, args, err := sqlx.In(`SELECT (CONVERT_TZ(i.created_on, '+00:00','-05:00')) AS last_invoice_date,
	i.bidder_id as user_id
FROM invoice AS i
WHERE i.active = true
AND i.bidder_id in(?)
AND i.invoice_status_id IN (1,2,3,4,5 )
AND IF((SELECT COUNT(1) FROM invoice_item AS ii WHERE ii.invoice_id = i.id
AND ii.hammer_price > 0 AND ii.active), true, false) > 0
ORDER BY i.created_on DESC;`, userIds)
	if err != nil {
		panic(err.Error())
	}
	query = db.Rebind(query)
	rows, err := db.Queryx(query, args...)
	if err != nil {
		panic(err.Error())
	}

	defer rows.Close()
	result := make(map[int64]string)

	for rows.Next() {
		var res LastUserInvoiceDateResult

		err = rows.StructScan(&res)

		if err != nil {
			panic(err.Error())
		}

		if res.UserId.Valid {
			result[res.UserId.Int64] = res.LastWinDate.String;
		}
	}

	c <- result
	close(c)
}

