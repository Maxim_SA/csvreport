package dao

//NO FINISHED
import (
	"github.com/jmoiron/sqlx"
	"database/sql"
)

type BiddersResult struct {
	Id        int64         `db:"id"`
	UserId    sql.NullInt64 `db:"user_id"`
	Preferred sql.NullInt64 `db:"preferred"`
}

func GetBiddersByUserId(db *sqlx.DB, c chan map[int64]BiddersResult, userIds []int64) {

	query, args, err := sqlx.In(`SELECT id, user_id, preferred
	FROM bidder b WHERE b.user_id in (?)`, userIds)
	if err != nil {
		panic(err.Error())
	}
	query = db.Rebind(query)
	rows, err := db.Queryx(query, args...)
	if err != nil {
		panic(err.Error())
	}

	defer rows.Close()
	result := make(map[int64]BiddersResult)

	for rows.Next() {
		var res BiddersResult

		err = rows.StructScan(&res)

		if err != nil {
			panic(err.Error())
		}

		if res.UserId.Valid {
			result[res.UserId.Int64] = res;
		}
	}

	c <- result
	close(c)
}
