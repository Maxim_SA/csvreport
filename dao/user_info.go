package dao

//NO FINISHED
import (
	"github.com/jmoiron/sqlx"
	"database/sql"
)

type UserInfoResult struct {
	Id             int64           `db:"id"`
	UserId         sql.NullInt64   `db:"user_id"`
	CompanyName    sql.NullString  `db:"company_name"`
	FirstName      sql.NullString  `db:"first_name"`
	LastName       sql.NullString  `db:"last_name"`
	SalesTax       sql.NullFloat64 `db:"sales_tax"`
	TaxApplication sql.NullInt64   `db:"tax_application"`
	Note           sql.NullString  `db:"note"`
	Phone          sql.NullString  `db:"phone"`
	NewsLetter     sql.NullInt64   `db:"news_letter"`
	Referrer       sql.NullString  `db:"referrer"`
	ReferrerHost   sql.NullString  `db:"referrer_host"`
}

func GetUserInfoByUserId(db *sqlx.DB, c chan map[int64]UserInfoResult, userIds []int64) {

	query, args, err := sqlx.In(`SELECT
		id,
		user_id,
		company_name,
		first_name,
		last_name,
		sales_tax,
		tax_application,
		note,
		phone,
		news_letter,
		referrer,
		referrer_host
	FROM user_info WHERE user_id IN (?)`, userIds)
	if err != nil {
		panic(err.Error())
	}
	query = db.Rebind(query)
	rows, err := db.Queryx(query, args...)
	if err != nil {
		panic(err.Error())
	}

	defer rows.Close()
	result := make(map[int64]UserInfoResult)

	for rows.Next() {
		var res UserInfoResult

		err = rows.StructScan(&res)

		if err != nil {
			panic(err.Error())
		}

		if res.UserId.Valid {
			result[res.UserId.Int64] = res;
		}
	}

	c <- result
	close(c)
}

