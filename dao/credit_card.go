package dao

import (
	"database/sql"
	"log"
	"github.com/jmoiron/sqlx"
)

func GetNameById(db *sqlx.DB, c chan string, id int, mainAccountId int) {
	var name sql.NullString;
	err := db.QueryRow(`SELECT * FROM credit_card
WHERE id = ? AND account_id = ? AND active = 1 LIMIT 0, 1`, id, mainAccountId).Scan(&name)
	switch {
	case err == sql.ErrNoRows:
		log.Printf("No user with that ID.")
	case err != nil:
		log.Fatal(err)
	}
	c <- name.String
	close(c)
}
