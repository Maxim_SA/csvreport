package dao

import (
	"database/sql"
	"github.com/jmoiron/sqlx"
)

type UserCustomDataResult struct {
	Id              int           `db:"id"`
	UserId          sql.NullInt64 `db:"user_id"`
	UserCustFieldId sql.NullInt64 `db:"user_cust_field_id"`
	Numeric   sql.NullString `db:"numeric"`
	Text      sql.NullString `db:"text"`
	Active    sql.NullInt64  `db:"active"`
	Encrypted sql.NullInt64  `db:"encrypted"`
}

func GetUserCustData(db *sqlx.DB, c chan map[int64]UserCustomDataResult, userCustomFieldIds []int, userIds []int64) {

	query, args, err := sqlx.In(`SELECT *
	FROM user_cust_data ucd
	WHERE user_id in (?) AND user_cust_field_id in (?) AND active = 1 `, userIds, userCustomFieldIds)
	if err != nil {
		panic(err.Error())
	}
	query = db.Rebind(query)
	rows, err := db.Queryx(query, args...)
	if err != nil {
		panic(err.Error())
	}

	defer rows.Close()
	result := make(map[int64]UserCustomDataResult)

	for rows.Next() {
		var res UserCustomDataResult

		err = rows.StructScan(&res)

		if err != nil {
			panic(err.Error())
		}

		if res.UserId.Valid {
			result[res.UserId.Int64] = res;
		}
	}

	c <- result
	close(c)
}
