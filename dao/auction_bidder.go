package dao

import (
	"database/sql"
	"github.com/jmoiron/sqlx"
)

type AuctionBidder struct {
	Id                   int             `db:"id"`
	AuctionId            sql.NullInt64   `db:"auction_id"`
	UserId               sql.NullInt64   `db:"user_id"`
	BidderNum            sql.NullString  `db:"bidder_num"`
	BidBudget            sql.NullFloat64 `db:"bid_budget"`
	RegisteredOn         sql.NullString  `db:"registered_on"`
	Approved             sql.NullInt64   `db:"approved"`
	IsReseller           sql.NullInt64   `db:"is_reseller"`
	ResellerApproved     sql.NullInt64   `db:"reseller_approved"`
	ResellerId           sql.NullString  `db:"reseller_id"`
	ResellerCertificate  sql.NullString  `db:"reseller_certificate"`
	AuthAmount           sql.NullFloat64 `db:"auth_amount"`
	AuthDate             sql.NullString  `db:"auth_date"`
	CarrierMethod        sql.NullString  `db:"carrier_method"`
	PostAucImportPremium sql.NullFloat64 `db:"post_auc_import_premium"`
	Spent                sql.NullFloat64 `db:"spent"`
	Collected            sql.NullFloat64 `db:"collected"`
	Referrer             sql.NullString  `db:"referrer"`
	ReferrerHost         sql.NullString  `db:"referrer_host"`
}

func GetBidders(db *sqlx.DB, c chan map[int]AuctionBidder, auctionId int) {
	rows, err := db.Queryx(`SELECT ab.*
	 FROM auction_bidder AS ab
	 LEFT JOIN auction AS a ON a.id = ab.auction_id
	 WHERE ab.user_id > 0
	 AND ab.auction_id = ?
	 ORDER BY ab.bidder_num DESC;`, auctionId)
	if err != nil {
		panic(err.Error())
	}
	result := make(map[int]AuctionBidder)

	for rows.Next() {
		var p AuctionBidder
		err = rows.StructScan(&p)
		if err != nil {
			continue
		}
		result[p.Id] = p
	}
	c <- result
	close(c)
}
