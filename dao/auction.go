package dao

import (
	"database/sql"
	"log"
	"github.com/jmoiron/sqlx"
)

type Auction struct {
	id        int
	accountId int
}

func GetAccountIdByAuction(db *sqlx.DB, c chan int, auctionId int) {
	var accountId int;
	err := db.QueryRow(`SELECT account_id FROM auction WHERE id = ? LIMIT 0, 1`, auctionId).Scan(&accountId)
	switch {
	case err == sql.ErrNoRows:
		log.Printf("No user with that ID.")
	case err != nil:
		log.Fatal(err)
	}
	c <- accountId
	close(c)
}
