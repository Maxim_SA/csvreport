package dao

import (
	"database/sql"
	"log"
	"github.com/jmoiron/sqlx"
)

func GetMainAccountId(db *sqlx.DB, c chan int) {
	var accountId int;
	err := db.QueryRow(`SELECT id FROM account WHERE active = 1 AND main = 1 LIMIT 0, 1`).Scan(&accountId)
	switch {
	case err == sql.ErrNoRows:
		log.Printf("No user with that ID.")
	case err != nil:
		log.Fatal(err)
	}
	c <- accountId
	close(c)
}
