package dao

import (
	"database/sql"

	"log"
	"github.com/jmoiron/sqlx"
	"github.com/jmoiron/sqlx/types"
)

type SysParams struct {
	DefaultExportEncoding sql.NullString `db:"default_export_encoding"`
	AchPayment            types.BitBool   `db:"ach_payment"`
}

func GetSysParams(db *sqlx.DB, c chan SysParams, accountId int) {
	var sysParams SysParams

	err := db.QueryRow(`SELECT default_export_encoding, ach_payment
 FROM auction_parameters
 WHERE account_id = ? LIMIT 0, 1`, accountId).
		Scan(&sysParams.DefaultExportEncoding, &sysParams.AchPayment)

	switch {
	case err == sql.ErrNoRows:
		log.Printf("No user with that ID.")
	case err != nil:
		log.Fatal(err)
	}
	c <- sysParams
	close(c)
}

