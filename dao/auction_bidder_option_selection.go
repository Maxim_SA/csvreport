package dao

//NO FINISHED
import (
	"github.com/jmoiron/sqlx"
	"database/sql"
)

type BiddersOptionsResult struct {
	UserId sql.NullInt64  `db:"user_id"`
	Name   sql.NullString `db:"name"`
	Option sql.NullString `db:"option"`
}

func GetBiddersOptions(db *sqlx.DB, c chan map[int64]BiddersOptionsResult, auctionId int, userIds []int64) {

	query, args, err := sqlx.In(`SELECT
    abos.user_id,
	abo.name,
    abos.option
FROM auction_bidder_option_selection abos LEFT JOIN auction_bidder_option AS abo
    ON abos.auction_bidder_option_id = abo.id
WHERE abos.auction_id = ? AND abos.user_id in (?)`, auctionId, userIds)
	if err != nil {
		panic(err.Error())
	}
	query = db.Rebind(query)
	rows, err := db.Queryx(query, args...)
	if err != nil {
		panic(err.Error())
	}

	defer rows.Close()
	result := make(map[int64]BiddersOptionsResult)

	for rows.Next() {
		var res BiddersOptionsResult

		err = rows.StructScan(&res)

		if err != nil {
			panic(err.Error())
		}

		if res.UserId.Valid {
			result[res.UserId.Int64] = res;
		}
	}

	c <- result
	close(c)
}
