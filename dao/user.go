package dao

import (
	"database/sql"
	"github.com/jmoiron/sqlx"
)

type UserResult struct {
	Id                   int64            `db:"id"`
	Username             sql.NullString `db:"username"`
	Email                sql.NullString `db:"email"`
	CustomerNo           sql.NullInt64  `db:"customer_no"`
	UsePermanentBidderno sql.NullInt64  `db:"use_permanent_bidderno"`
	CreatedBy            sql.NullInt64  `db:"created_by"`
	CreatedOn            sql.NullString `db:"created_on"`
	LogDate              sql.NullString `db:"log_date"`
	NameAddedBy          sql.NullString `db:"name_added_by"`
}

func GetUsers(db *sqlx.DB, c chan map[int64]UserResult, ids []int64) {

	query, args, err := sqlx.In(`SELECT
  u.id,
  u.username,
  u.email,
  u.customer_no,
  u.use_permanent_bidderno,
  u.created_by,
  u.created_on,
  u.log_date,
  ua.username AS name_added_by
FROM user AS u LEFT JOIN user AS ua ON u.added_by = ua.id
WHERE u.id IN (?)`, ids)
	if err != nil {
		panic(err.Error())
	}
	query = db.Rebind(query)
	rows, err := db.Queryx(query, args...)
	if err != nil {
		panic(err.Error())
	}

	defer rows.Close()
	result := make(map[int64]UserResult)

	for rows.Next() {
		var res UserResult

		err = rows.StructScan(&res)

		if err != nil {
			panic(err.Error())
		}
		result[res.Id] =  res;
	}

	c <- result
	close(c)
}
