package main

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"sync"
	"time"
	"github.com/namax/dbAsync/dao"
	"log"
	"fmt"
	"os"
	"encoding/csv"
)

var wg sync.WaitGroup

//app/admin/views/scripts/manage-auctions/bidder-export.tpl.php

func main() {
	db, err := sqlx.Connect("mysql", "root:1@tcp(192.168.30.105:3306)/auctionserver_dev")
	if err != nil {
		log.Fatalln(err)
	}

	if err != nil {
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}
	defer db.Close()
	// Open doesn't open a connection. Validate DSN data:
	err = db.Ping()
	if err != nil {
		panic(err.Error())
	}

	start := time.Now()

	auctionId := 4145

	auctionBidderChan := make(chan map[int]dao.AuctionBidder)
	accountChan := make(chan int)
	mainAccountChan := make(chan int)
	sysParamsChan := make(chan dao.SysParams)
	userCustomFieldChan := make(chan []dao.UserCustomField)

	go dao.GetMainAccountId(db, mainAccountChan)
	go dao.GetBidders(db, auctionBidderChan, auctionId)
	go dao.GetAccountIdByAuction(db, accountChan, auctionId)
	go dao.LoadAllEditable(db, userCustomFieldChan)

	accountId := <-accountChan
	mainAccountId := <-mainAccountChan

	go dao.GetSysParams(db, sysParamsChan, accountId)

	sysParamsResult := <-sysParamsChan

	userCustomFieldResult := <-userCustomFieldChan
	auctionBidderResult := <-auctionBidderChan

	//fmt.Println(userCustomFieldResult[4].Name.String)
	fmt.Println(accountId)
	fmt.Println(mainAccountId)

	fmt.Println(sysParamsResult)
	fmt.Println(userCustomFieldResult)

	var userIds []int64
	//auctionsIds := make(map[int]int64)

	for _, auctionBidder := range auctionBidderResult {
		if auctionBidder.UserId.Valid {
			userIds = append(userIds, auctionBidder.UserId.Int64)
		}
		//if auctionBidder.AuctionId.Valid {
		//	auctionsIds[auctionBidder.Id] = auctionBidder.AuctionId.Int64
		//}
	}
	fmt.Println(userIds)

	userChan := make(chan map[int64]dao.UserResult)
	allLastUsersBidDatesChan := make(chan map[int64]dao.LastUsersBidDatesResult)
	allLastUsersWinDatesChan := make(chan map[int64]dao.LastUsersWinDatesResult)
	allLastUsersPaymentChan := make(chan map[int64]dao.LastUsersPaymentDatesResult)
	allLastUserInvoiceDateChan := make(chan map[int64]string)
	allUserInfoChan := make(chan map[int64]dao.UserInfoResult)
	allUserBillingChan := make(chan map[int64]dao.UserBillingResult)
	allUserShippingChan := make(chan map[int64]dao.UserShippingResult)
	allBiddersOptionsChan := make(chan map[int64]dao.BiddersOptionsResult)
	allUserCustomDataChan := make(chan map[int64]dao.UserCustomDataResult)

	go dao.GetUsers(db, userChan, userIds)
	go dao.GetALLLastUsersBidDates(db, allLastUsersBidDatesChan, userIds)
	go dao.GetALLLastUsersWinDates(db, allLastUsersWinDatesChan, userIds)
	go dao.GetALLLastUsersPaymentDates(db, allLastUsersPaymentChan, userIds)
	go dao.GetLastUserInvoiceDate(db, allLastUserInvoiceDateChan, userIds)
	go dao.GetUserInfoByUserId(db, allUserInfoChan, userIds)
	go dao.GetUserBillingByUserId(db, allUserBillingChan, userIds)
	go dao.GetUserShippingByUserId(db, allUserShippingChan, userIds)
	go dao.GetBiddersOptions(db, allBiddersOptionsChan, auctionId, userIds)

	var userCustomFieldIds []int;
	var userCustomFieldNames []string;
	for _, userCustomField := range userCustomFieldResult {
		userCustomFieldIds = append(userCustomFieldIds, userCustomField.Id)
		if userCustomField.Name.Valid {
			userCustomFieldNames = append(userCustomFieldNames, userCustomField.Name.String)
		}
	}
	go dao.GetUserCustData(db, allUserCustomDataChan, userCustomFieldIds, userIds)

	userResult := <-userChan
	allLastUsersBidDatesResult := <-allLastUsersBidDatesChan
	allLastUsersWinDatesResult := <-allLastUsersWinDatesChan
	allLastUsersPaymentResult := <-allLastUsersPaymentChan
	allLastUserInvoiceDateResult := <-allLastUserInvoiceDateChan
	allUserInfoResult := <-allUserInfoChan
	allUserBillingResult := <-allUserBillingChan
	allUserShippingResult := <-allUserShippingChan
	allBiddersOptionsResult := <-allBiddersOptionsChan
	allUserCustomDataResult := <-allUserCustomDataChan

	fmt.Println(userResult)
	fmt.Println(allLastUsersBidDatesResult)
	fmt.Println(allLastUsersWinDatesResult)
	fmt.Println(allLastUsersPaymentResult)
	fmt.Println(allLastUserInvoiceDateResult)
	fmt.Println(allUserInfoResult)
	fmt.Println(allUserBillingResult)
	fmt.Println(allUserShippingResult)
	fmt.Println(allBiddersOptionsResult)
	fmt.Println(" ---")

	fmt.Println(allUserCustomDataResult)

	file, err := os.Create("result.csv")
	if err != nil {
		log.Fatal("Cannot create file", err)
	}
	defer file.Close()

	w := csv.NewWriter(file)
	//w := csv.NewWriter(os.Stdout)
	defer w.Flush()

	headers := []string{
		"Bidder #", "Username", "CompanyName", "Email", "FirstName", "LastName", "Phone #", "Customer #",
		"Buyer's Sales Tax (%)", "Apply Tax To", "Notes",
		"Billing Contact type", "Billing Company name", "Billing First name", "Billing Last name",
		"Billing Phone", "Billing Fax", "Billing Country", "Billing Address", "Billing Address Ln 2",
		"Billing Address Ln 3", "Billing City", "Billing State", "Billing ZIP",
		"CC Type", "CC Number", "CC Exp. Date",
		"Bank Routing #", "Bank Acct #", "Bank Acct Type", "Bank Name", "Bank Account Name",
		"Shipping Contact type", "Shipping Company name", "Shipping First name", "Shipping Last name",
		"Shipping Phone", "Shipping Fax", "Shipping Country", "Shipping Address", "Shipping Address Ln 2",
		"Shipping Address Ln 3", "Shipping City", "Shipping State", "Shipping ZIP",
		"Auction Registration Date", "Registration Date", "Last Bid Date", "Last Win Date",
		"Last Invoice Date", "Last Payment Date", "Last Login Date", "Credit Card?", "Bidder?",
		"Preferred Bidder?", "Newsletter?", "Make permanent bidder number?", "Referrer", "Referrer Host"}

	headers = append(headers, userCustomFieldNames...)
	headers = append(headers, []string{"User added by", "Bidder Option Selection"}...)

	if err := w.Write(headers); err != nil {
		log.Fatalln("error writing record to csv:", err)
	}
	for _, auctionBidder := range auctionBidderResult {
		if !auctionBidder.UserId.Valid {
			continue;
		}

		var record []string;
		if userData, ok := userResult[auctionBidder.UserId.Int64]; ok {
			fmt.Println(userData)

			record = append(record, auctionBidder.BidderNum.String)
			record = append(record, userData.Username.String)

			var companyName = ""
			if userInfo, ok := allUserInfoResult[auctionBidder.UserId.Int64]; ok {
				companyName = userInfo.CompanyName.String
			}
			record = append(record, companyName)
			record = append(record, userData.Email.String)


			if err := w.Write(record); err != nil {
				log.Fatalln("error writing record to csv:", err)
			}
		}

	}

	// Write any buffered data to the underlying writer (standard output).
	w.Flush()

	if err := w.Error(); err != nil {
		log.Fatal(err)
	}

	elapsed := time.Since(start)
	fmt.Printf("Time %s\n", elapsed)
	fmt.Println("-- end --")
}
